using System;   
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SEProject1.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        public delegate List<Student> GetGpa(double gpa);
        public List<Student> students;

        public StudentController()
        {
            students = new List<Student>();

            Student st1 = new Student()
            {
                Id = 1,
                Name = "Muratzhan",
                Surname = "Adylkanov",
                Age = 18,
                Birthday = new DateTime(2002, 06, 12)
            };
            Student st2 = new Student()
            {
                Id = 2,
                Name = "Bogenbay",
                Surname = "Batyr",
                Age = 98,
                Birthday = new DateTime(1690, 06, 12)
            };
            Student st3 = new Student()
            {
                Id = 3,
                Name = "Kabanbay",
                Surname = "Batyr",
                Age = 78,
                Birthday = new DateTime(1692, 06, 12)
            };
            Student st4 = new Student()
            {
                Id = 4,
                Name = "Saken",
                Surname = "Seifullin",
                Age = 45,
                Birthday = new DateTime(1984, 10, 15)
            };
            Student st5 = new Student()
            {
                Id = 5,
                Name = "Abay",
                Surname = "Kunanbayev",
                Age = 58,
                Birthday = new DateTime(1845, 09, 10)
            };

            students.Add(st1);
            students.Add(st2);
            students.Add(st3);
            students.Add(st4);
            students.Add(st5);
        }

        [HttpGet("/students")]

        public List<Student> getStudents()
        {   
            return students;
        }

        [HttpGet("/students/{name}")]
        public List<Student> getStudentsByName(string name)
        {
            var list = students.Where(student => student.Name.ToLower() == name.ToLower()).ToList();
            return list;
        }
        [HttpGet("/students/id/{id}")]
        public List<Student> getStudentsById(int id)
        {
            var list = students.Where(student => student.Id == id).ToList();
            return list;
        }

        [HttpGet("/students/age/{start}-{finish}")]
        public List<Student> getStudentsByAgeInterval(int start, int finish)
        {
            var list = students.Where(student => student.Age >= start && student.Age <= finish).ToList();
            return list;
        }
        [HttpGet("/students/birthday/{dateTimeStart}.{dateTimeFinish}")]
        public List<Student> getStudentByBirthdayInterval(DateTime dateTimeStart, DateTime dateTimeFinish)
        {

            var list = students.Where(student => student.Birthday.Day > dateTimeStart.Day && student.Birthday.Day < dateTimeFinish.Day &&
                                        student.Birthday.Month >= dateTimeStart.Month && student.Birthday.Month <= dateTimeStart.Month &&
                                        student.Birthday.Year >= dateTimeStart.Year && student.Birthday.Year <= dateTimeStart.Year).ToList();
            return list;
        }

        [HttpGet("/students/gpa/{gpa}")]
        public List<Student> getStudentsByGpa(GetGpa _del, double gpa)
        {
            _del = showGpa;
            var list = _del?.Invoke(gpa);
            return list;
        }
        public List<Student> showGpa(double gpa)
        {
            var list = students.Where(student => student.Gpa >= gpa).ToList();
            return list;
        }
    }
}
